import numpy as np 
import math
import cmath
from numpy import linalg as LA

def get_ground_state(num_qubits):
    # return vector of size 2**num_qubits with all zeroes except first element which is 1
    g_state = [0]* (2 ** num_qubits)
    g_state[0] = 1 
    return g_state


def get_operator(total_qubits, gate_unitary, target_qubits, theta=3.1415, phi=1.5708, Lambda = -3.1415):
    # return unitary operator of size 2**n x 2**n for given gate and target qubits
    # A set of single-qbit 2*2 gates
        # their tensor products with each other and identities gives the final operator  
    I = np.identity(2)
    P0x0 = np.array([
            [1, 0],
            [0, 0]
            ])
    P1x1 = np.array([
            [0, 0],
            [0, 1]
            ])
    H = 1/np.sqrt(2) * np.array([
            [1, 1],
            [1, -1]
            ])
    X = np.array([
            [0, 1],
            [1, 0]
            ])
    Z = np.array([
            [1, 0],
            [0, -1]
            ])
    U3 = np.array([
            [math.cos(theta/2), -cmath.exp(1j * Lambda) * math.sin(theta / 2)],
            [cmath.exp(1j * phi) * math.sin(theta / 2), cmath.exp(1j * Lambda + 1j * phi) * math.cos(theta / 2)]
            ])
    # If the unitary gate required is a single-qbit gate
    if (len(target_qubits) == 1):
        target = target_qubits[0]
        if(gate_unitary == "h"):
            Gate = H 
        elif(gate_unitary == "x"):
            Gate = X 
        elif(gate_unitary == "z"):
            Gate = Z
        elif(gate_unitary == "u3"):
            Gate = U3
        elif(gate_unitary == "e"):
            Gate = [X,Z,I][np.random.choice(range(3), p = [0.3,0.3,0.4])]
        else:
            print("Unknown single-qubit gate")
            Gate = None 
        # Take tensor products of the single-qbit gate with identities
        # eg. I * ... * I * X * I * .. * I
        # Op is the final 2**n x 2**n matrix     
        Op = np.kron(np.kron(np.identity(2**(target)), Gate), np.identity(2**(total_qubits-1-target)))        
    # CNOT gate 
        # The final Op takes the form I * ... * I * Control * I * ... * I * Target * I * .. * I
    if (len(target_qubits) == 2):
        if (gate_unitary == "cx"):
            tGate = X
        elif (gate_unitary == "cz"):
            tGate = Z
        else:
            print("Unknown control gate")
            tGate = None
        control = target_qubits[0]
        target = target_qubits[1]
        if(control < target):     
            Gate =  np.kron(np.kron(P0x0, np.identity(2**(target-control-1))), I) + np.kron(np.kron(P1x1, np.identity(2**(target-control-1))), tGate)
            Op = np.kron(np.kron(np.identity(2**control), Gate), np.identity(2**(total_qubits-1-target)))
        elif(control > target):
            Gate =  np.kron(np.kron(I, np.identity(2**(target-control-1))), P0x0) + np.kron(np.kron(tGate, np.identity(2**(target-control-1))), P1x1) 
            Op = np.kron(np.kron(np.identity(2**target), Gate), np.identity(2**(total_qubits-1-control)))
    return Op 

def run_program(initial_state, program):
    # read program, and for each gate:
    #   - calculate matrix operator
    #   - multiply state with operator
    # return final state
    curr_state = initial_state
    n_qbit = math.log(len(initial_state)+1, 2) 
    for gate in program:
        if "params" in gate:
            Op = get_operator(n_qbit, gate["gate"], gate["target"],**gate["params"])
        curr_state = np.dot(curr_state, Op)
    return curr_state

def applitude(state_vector):
    dict={}
    for i in range(len(state_vector)):
        if state_vector[i] != 0:
            bin_index = bin(i)[2:]
            makeup_zeros = int(math.log(1+len(state_vector),2) - len(bin_index))
            final_bin_index = '0' * makeup_zeros + bin_index
            dict[final_bin_index] = state_vector[i]
    return dict

def measure_all(state_vector):
    # Probability distribution to be used in the weighted random sampling 
    prob = np.multiply(np.conj(state_vector), state_vector) 
    # choose element from state_vector using weighted random and return it's index
    index = np.random.choice(range(len(state_vector)),p = prob)
    # Translate to binary representations
    bin_index = bin(index)[2:]
    makeup_zeros = int(math.log(1+len(state_vector),2) - len(bin_index))
    final_bin_index = '0' * makeup_zeros + bin_index
    return final_bin_index

   
def get_counts(state_vector, num_shots):
    # simply execute measure_all in a loop num_shots times and
    # return object with statistics in following form:
    #   {
    #      element_index: number_of_ocurrences,
    #      element_index: number_of_ocurrences,
    #      element_index: number_of_ocurrences,
    #      ...
    #   }
    # (only for elements which occoured - returned from measure_all)
    dict = {}
    for i in range(num_shots):
        key = measure_all(state_vector)
        if key not in dict:
            dict[key] = 1
        else: 
            dict[key] += 1
    return dict


def measure_single(state_vector,target):
    total_qubits = math.log(1+len(state_vector),2) 
    P0x0 = np.array([
            [1, 0],
            [0, 0]
            ])
    P1x1 = np.array([
            [0, 0],
            [0, 1]
            ])
    Proj0 = np.kron(np.kron(np.identity(2**(target)), P0x0), np.identity(2**(total_qubits-1-target)))        
    Proj1 = np.kron(np.kron(np.identity(2**(target)), P1x1), np.identity(2**(total_qubits-1-target)))        
    norm0 = LA.norm(np.dot(state_vector, Proj0))
    norm1 = LA.norm(np.dot(state_vector, Proj1))    
    single_vec = np.array([norm0, norm1])
    return 1.0 - single_vec[0]
    

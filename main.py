import simulator as sim
import math

#Task 3: QC simulator

# Parameters used by the parametric gates
Global_1 = math.pi
Global_2 = math.pi/2

my_circuit = [
#{ "gate": "h", "target": [0], "params":{} }, 
#{ "gate": "cx", "target": [0, 1] , "params":{}},
{ "gate": "u3", "params": { "theta": Global_1, "phi": Global_2, "Lambda": -math.pi }, "target": [0] },
#{ "gate": "e", "target":[0] , "params":{} }
]


# Create "quantum computer" with 2 qubits 

my_qpu = sim.get_ground_state(3)


# Run circuit

final_state = sim.run_program(my_qpu, my_circuit)


# Read results

counts = sim.get_counts(final_state, 1000)


def objective_function(params):
    global_1 = params[0]
    global_2 = params[1]
    my_circuit = [
                  { "gate": "h", "target": [0], "params":{} }, 
                  { "gate": "cx", "target": [0, 1] , "params":{}},
                  { "gate": "u3", "params": { "theta": global_1, "phi": global_2, "Lambda": -math.pi }, "target": [0] }
                ]
    final_state = sim.run_program(my_qpu, my_circuit) 
    counts = sim.get_counts(final_state, 1000)

    # ...calculate cost here...
    # Not completed 
    # Right now just return counts from any choice of theta and phi

    return counts 

###########################################
# Task2: Erroneous Bell state preparation # 
###########################################

# Bell state by Hadamard and CNOT on \ket{00}
bell_circuit= [
{ "gate": "h", "target": [0], "params":{} },
{ "gate": "cx", "target": [0,1], "params":{} }
]

simple_ground_state = sim.get_ground_state(2)
simple_bell_state = sim.run_program(simple_ground_state, bell_circuit)
print "Ideal bell state preparation:\n", sim.applitude(simple_bell_state),"\n"

# Errors happen between Hadamard and CNOT 
error_bell_circuit= [
{ "gate": "h", "target": [0], "params":{} },
{ "gate": "e", "target": [0], "params":{} },
{ "gate": "e", "target": [1], "params":{} },
{ "gate": "cx", "target": [0,1], "params":{} }
]
error_bell_state = sim.run_program(simple_ground_state, error_bell_circuit)
print "An erroneous bell state preparation:\n",sim.applitude(error_bell_state),"\n"

# Bit-flip encoding a\ket{0} + b\ket{1} -> a\ket{000} + b\ket{111}
bit_flip_code = [
{ "gate": "cx", "target": [0,1], "params":{} },
{ "gate": "cx", "target": [0,2], "params":{} }
]
para_circuit = [
{ "gate": "u3", "params": { "theta":math.pi/3 , "phi": math.pi/2, "Lambda": -math.pi }, "target": [0] },
]
encoding_ground_state = sim.get_ground_state(3)
any_state = sim.run_program(encoding_ground_state, para_circuit)
encode_anystate = sim.run_program(any_state, bit_flip_code)
print "An arbituary state to encode:\n",sim.applitude(any_state)
print "After bit flip encoding:\n", sim.applitude(encode_anystate),"\n"

# Encoded bell state praperation 
coded_bell_circuit = [
{ "gate": "h", "target": [0], "params":{} },
{ "gate": "cx", "target": [0,1], "params":{} },
{ "gate": "cx", "target": [0,2], "params":{} },
{ "gate": "e", "target": [0], "params":{} },
{ "gate": "e", "target": [1], "params":{} },
{ "gate": "e", "target": [2], "params":{} },
{ "gate": "e", "target": [3], "params":{} },
{ "gate": "e", "target": [4], "params":{} },
{ "gate": "e", "target": [5], "params":{} },
{ "gate": "cx", "target": [0,3], "params":{} },
{ "gate": "cx", "target": [0,4], "params":{} },
{ "gate": "cx", "target": [0,5], "params":{} }
]

def measure_2Z(curr_state, target_bit):
    m_circuit = [
    { "gate": "cx", "target": [target_bit[0],6], "params":{} },
    { "gate": "cx", "target": [target_bit[1],6], "params":{} }
    ]
    measure_state = sim.run_program(curr_state, m_circuit)
    meas_ac = sim.measure_single(measure_state,6)
    return meas_ac

def measure_6X(curr_state):
    m_circuit = [
    { "gate": "h", "target": [6], "params":{} }, 
    { "gate": "cx", "target": [0,5], "params":{} },
    { "gate": "cx", "target": [0,4], "params":{} },
    { "gate": "cx", "target": [0,3], "params":{} },
    { "gate": "cx", "target": [0,2], "params":{} },
    { "gate": "cx", "target": [0,1], "params":{} },
    { "gate": "h", "target": [0], "params":{} },
    { "gate": "cz", "target": [0,6], "params":{} },
    { "gate": "h", "target": [6], "params":{} }
    ]
    measure_state = sim.run_program(curr_state, m_circuit)
    #print curr_state
    meas_ac = sim.measure_single(measure_state,6)
    return meas_ac
    
def prep_logical_bell_state():
    logic_bits = sim.get_ground_state(7)   
    error_state = sim.run_program(logic_bits, coded_bell_circuit) 
    print "logical bits after erroneous preparation before correction:\n", sim.applitude(error_state)
    z01 = measure_2Z(error_state,[0,1])
    z12 = measure_2Z(error_state,[1,2])
    if (z01 > 1.0 -  1e-15 and z12 < 1e-15):
        correct_circuit = [
                            { "gate": "x", "target": [0], "params":{} },
                            { "gate": "x", "target": [3], "params":{} }
                          ]
        error_state = sim.run_program(error_state, correct_circuit) 
    elif (z01 > 1.0 - 1e-15 and z12 > 1 - 1e-15):
        correct_circuit = [
                            { "gate": "x", "target": [1], "params":{} },
                            { "gate": "x", "target": [4], "params":{} }
                          ]
        error_state = sim.run_program(error_state, correct_circuit) 
    elif (z01 < 1e-15  and z12 > 1 - 1e-15):
        correct_circuit = [
                             { "gate": "x", "target": [2], "params":{} },
                             { "gate": "x", "target": [5], "params":{} }
                           ]
        error_state = sim.run_program(error_state, correct_circuit) 
    z34 = measure_2Z(error_state,[3,4])
    z45 = measure_2Z(error_state,[4,5])
    if (z34 > 1.0 -  1e-15 and z45 < 1e-15):
        correct_circuit = [
                            { "gate": "x", "target": [3], "params":{} }
                          ]
        error_state = sim.run_program(error_state, correct_circuit) 
    elif (z34 > 1.0 -  1e-15 and z45 > 1.0 -  1e-15):
        correct_circuit = [
                            { "gate": "x", "target": [4], "params":{} }
                          ]
        error_state = sim.run_program(error_state, correct_circuit) 
    elif (z34 < 1e-15 and z45 > 1.0 -  1e-15):
        correct_circuit = [
                             { "gate": "x", "target": [5], "params":{} }
                           ]
        error_state = sim.run_program(error_state, correct_circuit) 
    z03 = measure_2Z(error_state,[0,3])
    if (z03 > 1.0 -  1e-15):
        correct_circuit = [
                             { "gate": "x", "target": [3], "params":{} },
                             { "gate": "x", "target": [4], "params":{} },
                             { "gate": "x", "target": [5], "params":{} }
                           ]
        error_state = sim.run_program(error_state, correct_circuit)
    x_all = measure_6X(error_state)
    if (x_all > 1.0 -  1e-15):
        correct_circuit = [
                              { "gate": "z", "target": [0], "params":{} }
                            ]
        final_state = sim.run_program(error_state, correct_circuit)
    elif (x_all < 1e-15):
        final_state = error_state
    print "corrected logical state:\n",sim.applitude(final_state),"\n"
    return final_state


corrected_bell_state = prep_logical_bell_state()

decoding_circuit = [{ "gate": "cx", "target": [0,1], "params":{} },
                    { "gate": "cx", "target": [0,2], "params":{} },
                    { "gate": "cx", "target": [3,4], "params":{} },
                    { "gate": "cx", "target": [3,5], "params":{} }]
decoded_bell_state = sim.run_program(corrected_bell_state, decoding_circuit)
print "After decoding:\n", sim.applitude(decoded_bell_state)


for i in range(10):
    prep_logical_bell_state()

